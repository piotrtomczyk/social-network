#!/usr/bin/env python

import sys

import json
import requests as req

URL = 'https://api.instagram.com/v1/media/{}?access_token={}'

uid = sys.argv[1]
token = sys.argv[2]

res = req.get(URL.format(uid, token))
data = res.json()['data']['tags']

data = [u'#{}'.format(tag) for tag in data]

print json.dumps(data)
