utils = require 'utils'
fs = require 'fs'

casper = new (require 'casper').create
  loadImages: false
  loadPlugins: false
  loglevel: 'debug'
  verbose: true
  pageSettings:
    userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_5_8) AppleWebKit/534.50.2 (KHTML, like Gecko) Version/5.0.6 Safari/533.22.3'

COOKIE_JAR = 'cookies.txt'
phantom.cookies = JSON.parse fs.read COOKIE_JAR

id = casper.cli.args[0] or 'me'
fb = 'https://www.facebook.com/'

url = "#{fb}#{id}"

evaluate = (fb) ->
  selector = '#pagelet_timeline_medley_friends ' +
    'ul[data-pnref=friends] li ' +
    '.uiProfileBlockContent div.fsl.fwb.fcb a'
  list = document.querySelectorAll selector
  list = Array::slice.call list
  list
  .filter (a) ->
    a.attributes['data-hovercard']?.value.indexOf('user.php') >= 0
  .map (a) ->
    value = a.attributes['data-hovercard'].value
    from = value.indexOf('=') + 1
    to = value.indexOf('&') - 1
    displayName: a.textContent
    id: value[from .. to]

casper.start url

casper.then ->
  if @getCurrentUrl().indexOf('profile.php') >= 0
    friends = '&sk=friends'
  else
    friends = '/friends'
  @open "#{@getCurrentUrl()}#{friends}"

casper.then ->
  utils.dump @evaluate evaluate, fb

casper.run()
