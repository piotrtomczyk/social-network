fs = require 'fs'
utils = require 'utils'

casper = new (require 'casper').create
  loadImages: false
  loadPlugins: false
  loglevel: 'debug'
  verbose: true
  pageSettings:
    userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_5_8) AppleWebKit/534.50.2 (KHTML, like Gecko) Version/5.0.6 Safari/533.22.3'

COOKIE_JAR = 'cookies.txt'
phantom.cookies = JSON.parse fs.read COOKIE_JAR

id = casper.cli.args[0] or 'me'
fb = 'https://www.facebook.com/'

url = "#{fb}#{id}"

casper.start url, ->
  content = @evaluate -> (document.querySelector 'meta[property="al:android:url"]').content
  utils.dump id: content['fb://profile/'.length..]

casper.run()
