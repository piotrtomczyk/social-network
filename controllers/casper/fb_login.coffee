dump = (require 'utils').dump
fs = require 'fs'

COOKIE_JAR = 'cookies.txt'
phantom.cookies = {}

casper = new (require 'casper').create
  loadImages: false
  loadPlugins: false
  loglevel: 'debug'
  verbose: true
  pageSettings:
    userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_5_8) AppleWebKit/534.50.2 (KHTML, like Gecko) Version/5.0.6 Safari/533.22.3'

email = casper.cli.args[0]
pass = casper.cli.args[1]

casper.start 'https://www.facebook.com/', ->
  input = 'input[type=submit]'
  @fill 'form#login_form',
    'email': email
    'pass': pass
  @mouse.move input
  @click input
  @wait 5000, ->
    fs.write COOKIE_JAR, JSON.stringify phantom.cookies

casper.run()
