spawn = (require 'child_process').spawn
path = require 'path'

Promise = require 'bluebird'
request = require 'superagent'

api = (id, url='') ->
  "https://graph.facebook.com/v2.0/#{id}#{url}"

casper = (script, id) ->
  promise = new Promise (resolve, reject) ->
    child = spawn 'casperjs', [script, id],
      cwd: path.join __dirname, 'casper'
    data = ''
    child.stdout.on 'data', (buffer) -> data += buffer
    child.stderr.on 'data', (buffer) -> data += buffer
    child.on 'close', -> resolve data
  promise
  .then (buffer) -> JSON.parse buffer.toString()

module.exports =

  user: (id, query) ->
    request.get api id
    .query query
    .endAsync()
    .then (res) -> JSON.parse res.text

  statuses: (id, query) ->
    request.get api id, '/statuses'
    .query query
    .endAsync()
    .then (res) -> (JSON.parse res.text).data
    .then (res) ->
      res
      .filter (status) ->
        status.message?
      .map (status) ->
        id: status.id
        message: status.message

  tags: (id, query) ->
    request.get api id
    .query query
    .endAsync()
    .then (res) -> (JSON.parse res.text)
    .then (status) ->
      status.message.match /(#[a-z]+)/ig

  friends: (id, query) ->
    casper 'fb_friends.coffee', id

  id: (id, query) ->
    casper 'fb_to_id.coffee', id
