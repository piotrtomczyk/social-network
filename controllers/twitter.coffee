Promise = require 'bluebird'
Twitter = require 'twitter'

Config = require '../config.json'

client = new Twitter
  consumer_key: Config.TWITTER_API_KEY,
  consumer_secret: Config.TWITTER_API_SECRET,
  access_token_key: Config.TWITTER_TOKEN,
  access_token_secret: Config.TWITTER_TOKEN_SECRET,

Promise.promisifyAll client, suffix: 'Async'

module.exports =

  friends: (id) ->
    client.getAsync 'friends/list', user_id: id
    .then (res) -> res[0].users
    .then (users) ->
      users.map (user) ->
        id: user.id_str
        name: user.name

  statuses: (id) ->
    client.getAsync 'statuses/user_timeline', user_id: id
    .then (res) -> res[0]
    .then (statuses) ->
      statuses.map (status) ->
        id: status.id_str
        text: status.text

  tags: (id) ->
    client.getAsync "statuses/show/#{id}"
    .then (res) -> res[0]
    .then (status) ->
      status.entities.hashtags.map (tag) ->
        "#{tag.text}"
