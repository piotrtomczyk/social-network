spawn = (require 'child_process').spawn
path = require 'path'

Promise = require 'bluebird'

python = (script, id, token) ->
  promise = new Promise (resolve, reject) ->
    child = spawn 'python', [script, id, token],
      cwd: path.join __dirname, 'python'
    data = ''
    child.stdout.on 'data', (buffer) -> data += buffer
    child.stderr.on 'data', (buffer) -> data += buffer
    child.on 'close', -> resolve data
  promise
  .then (buffer) -> JSON.parse buffer.toString()

module.exports =
  follows: (id, token) ->
    python 'ig_friends.py', id, token

  media: (id, token) ->
    python 'ig_media.py', id, token
    .then (media) ->
      media.map (obj) ->
        id: obj.id
        caption: obj.caption.text

  tags: (id, token) ->
    python 'ig_tags.py', id, token
