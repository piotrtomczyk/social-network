React = require 'react'
Promise = require 'bluebird'

App = require './components/App.cjsx'

Promise.promisifyAll require 'superagent'

React.render <App />, document.body
