request = require 'superagent'

AppDispatcher = require '../dispatcher/AppDispatcher.coffee'
NodeConstants = require '../constants/NodeConstants.coffee'

NodeActions =
  addNode: (node) ->
    AppDispatcher.dispatch
      actionType: NodeConstants.ADD_NODE
      node: node
  fetchChildren: (method, params) ->
    request.get "/api/#{params.node.service}/#{method}?id=#{params.node.id}"
    .endAsync()
    .then (res) -> JSON.parse res.text
    .then (res) ->
      nodes = {}
      links = []

      type = switch method
        when 'statuses'
          NodeConstants.TYPE_DATA
        when 'friends'
          NodeConstants.TYPE_USER
        else
          NodeConstants.TYPE_TAG

      if type == NodeConstants.TYPE_TAG
        res = res.map (tag) ->
          id: (encodeURIComponent tag).replace /%/g, ''
          name: tag

      res.forEach (node) ->
        node.type = type
        node.service = params.node.service
        nodes[node.id] = node
        link =
          source: params.node.id
          target: node.id
        links.push link

      AppDispatcher.dispatch
        actionType: NodeConstants.NODES_FETCH
        caller: params.node
        nodes: nodes
        links: links
        method: method

module.exports = NodeActions
