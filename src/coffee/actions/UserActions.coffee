request = require 'superagent'

AppDispatcher = require '../dispatcher/AppDispatcher.coffee'
UserConstants = require '../constants/UserConstants.coffee'

UserActions =
  fetch: ->
    request.get '/api/user'
    .endAsync (res) ->
      AppDispatcher.dispatch
        actionType: UserConstants.USER_FETCH
        user: JSON.parse res.text

module.exports = UserActions
