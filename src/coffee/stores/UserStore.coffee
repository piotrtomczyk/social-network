EventEmitter = (require 'events').EventEmitter
Immutable = require 'immutable'
assign = require 'object-assign'

AppDispatcher = require '../dispatcher/AppDispatcher.coffee'
UserActions = require '../actions/UserActions.coffee'
UserConstants = require '../constants/UserConstants.coffee'

CHANGE_EVENT = 'CHANGE_EVENT'

UserRecord = Immutable.Record
  _id: null
  user:
    fb: undefined
    ig: undefined
    tt: undefined

user = new UserRecord

UserStore = assign {}, EventEmitter.prototype,
  getUser: ->
    user
  emitChange: ->
    @emit CHANGE_EVENT
  addChangeListener: (callback) ->
    @on CHANGE_EVENT, callback
  removeChangeListener: (callback) ->
    @removeListener CHANGE_EVENT, callback

AppDispatcher.register (action) ->
  switch action.actionType
    when UserConstants.USER_FETCH
      user = action.user
      UserStore.emitChange()
    else
      null
  true

module.exports = UserStore
