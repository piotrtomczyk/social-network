EventEmitter = (require 'events').EventEmitter
Immutable = require 'immutable'
assign = require 'object-assign'

AppDispatcher = require '../dispatcher/AppDispatcher.coffee'
NodeActions = require '../actions/NodeActions.coffee'
NodeConstants = require '../constants/NodeConstants.coffee'

CHANGE_EVENT = 'CHANGE_EVENT'

nodes = Immutable.Map()
links = Immutable.Set()

NodeStore = assign {}, EventEmitter.prototype,
  getNodes: ->
    nodes.valueSeq()
  getNodesMap: ->
    nodes
  getLinks: ->
    links
  emitChange: ->
    @emit CHANGE_EVENT
  addChangeListener: (callback) ->
    @on CHANGE_EVENT, callback
  removeChangeListener: (callback) ->
    @removeListener CHANGE_EVENT, callback

AppDispatcher.register (action) ->
  switch action.actionType
    when NodeConstants.ADD_NODE
      nodes = nodes.set action.node.id, action.node
      NodeStore.emitChange()
    when NodeConstants.NODES_FETCH
      debugger
      newNodes = Immutable.Map action.nodes
      newLinks = Immutable.Set action.links

      nodes = nodes.merge newNodes
      links = links.union newLinks

      caller = nodes.get action.caller.id
      caller["#{action.method}Fetched"] = true
      nodes.set action.caller.id, caller

      NodeStore.emitChange()
    else
      null
  true

module.exports = NodeStore
