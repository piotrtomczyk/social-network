React = require 'react'

NodeActions = require '../actions/NodeActions.coffee'
NodeConstants = require '../constants/NodeConstants.coffee'
NodeStore = require '../stores/NodeStore.coffee'
UserStore = require '../stores/UserStore.coffee'

NodeNav = React.createClass
  displayName: 'NodeNav'

  componentWillMount: ->
    UserStore.addChangeListener @_onUserStoreChange

  componentWillUnmount: ->
    UserStore.removeChangeListener @_onUserStoreChange

  getInitialState: ->
    id: ''

  render: ->

    id = @state.id
    buttons = @props.services.map (service) =>
      handler = =>
        NodeActions.addNode service: service.code, id: id, type: NodeConstants.TYPE_USER
        @setState id: ''
      <button 
        disabled={id.trim() == ''}
        onClick={handler}>
          Dodaj użytkownika {service.name}
      </button>

    <nav style={{@_navStyle}}>
      <input
        onChange={@_change}
        placeholder='podaj id'
        value={@state.id} />
      {buttons}
    </nav>

  _onUserStoreChange: ->
    @setState id: UserStore.getUser().tt.id

  _change: (event) ->
    @setState id: event.target.value

Node = React.createClass
  displayName: 'Node'

  render: ->

    buttons = @_getButtons()

    <li>
      {@props.node.name or @props.node.id} {buttons}
    </li>

  _getButtons: ->
    params =
      node: @props.node
    switch @props.node.type
      when NodeConstants.TYPE_USER
        friends = -> NodeActions.fetchChildren 'friends', params
        posts = -> NodeActions.fetchChildren 'statuses', params
        [
          <button disabled={@props.node.friendsFetched} key='friends' onClick={friends}>znajomi</button>
          <button disabled={@props.node.statusesFetched} key='posts' onClick={posts}>posty</button>
        ]
      when NodeConstants.TYPE_DATA
        tags = -> NodeActions.fetchChildren 'tags', params
        <button disabled={@props.node.tagsFetched} key='tags' onClick={tags}>tagi</button>
      else
        null


NodeList = React.createClass

  displayName: 'NodeList'

  getInitialState: ->
    nodes: NodeStore.getNodes()

  componentWillMount: ->
    NodeStore.addChangeListener @_onNodeStoreChange

  componentWillUnmount: ->
    NodeStore.removeChangeListener @_onNodeStoreChange

  render: ->

    items = @state.nodes.map (node) ->
      <Node key="#{node.service}:#{node.id}" node={node}/>

    <div style={@_style()}>
      <NodeNav services={@props.services} />
      <ul style={{ 'list-style-type': 'none' }}>
        {items}
      </ul>
    </div>

  _onNodeStoreChange: ->
    @setState nodes: NodeStore.getNodes()
  _style: ->
    bottom: 0
    display: 'block'
    overflow: 'auto'
    position: 'fixed'
    right: 0
    top: 0
    width: '400px'

module.exports = NodeList
