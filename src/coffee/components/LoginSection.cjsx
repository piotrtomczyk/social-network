React = require 'react'

LoginSection = React.createClass
  displayName: 'LoginSection'
  render: ->
    if @props.user
      <li>Logged in as {@props.user.displayName}</li>
    else
      <li><a href={"/auth/#{@props.link}"}>Login using {@props.service}</a></li>

module.exports = LoginSection
