React = require 'react'
d3 = require 'd3'

NodeStore = require '../stores/NodeStore.coffee'

force = null
link = null
links = null
node = null
nodes = null
svg = null

restart = ->
  link = link.data links
  link.enter()
    .insert 'line', '.node'
    .attr 'class', 'link'

  node = node.data nodes
  n = node.enter()
    .append 'g'
    .attr 'class', (d) ->
      type = (d.type.split '_')[1].toLowerCase()
      "#{type} node"

  n.append 'circle'
    .attr 'r', 5

  n.append 'text'
    .text (d) -> d.name or d.displayName or d.id

  force.start()

tick = ->
  link.attr 'x1', (d) -> d.source.x
    .attr 'y1', (d) -> d.source.y
    .attr 'x2', (d) -> d.target.x
    .attr 'y2', (d) -> d.target.y
  node
    .attr 'transform', (d) -> "translate(#{d.x}, #{d.y})"

createGraph = (dom, props, state) ->
  unless svg
    svg = d3.select dom
      .append 'svg'
      .attr 'width', props.width
      .attr 'height', props.height
    svg.append 'rect'
      .attr 'width', props.width
      .attr 'height', props.height

  _links = state.links.toArray()
  _nodes = state.nodes.toJS()

  _keys = []
  _links.forEach (link) ->
    obj =
      source: _nodes[link.source]
      target: _nodes[link.target]
    _keys.push obj
  _values = (_nodes[k] for k of _nodes)

  force = d3.layout.force()
    .size [props.width, props.height]
    .linkDistance 60
    .nodes _values
    .links _keys
    .on 'tick', tick
    .charge -300
  nodes = force.nodes()
  links = force.links()
  node = svg.selectAll '.node'
  link = svg.selectAll '.link'
  restart()

Graph = React.createClass
  displayName: 'Graph'

  getInitialState: ->
    nodes: NodeStore.getNodesMap()
    links: NodeStore.getLinks()

  getDefaultProps: ->
    width: 800
    height: 600

  componentDidMount: ->
    dom = @getDOMNode()
    createGraph dom, @props, @state

  componentWillMount: ->
    NodeStore.addChangeListener @_onNodeStoreChange

  componentWillUnmount: ->
    NodeStore.removeChangeListener @_onNodeStoreChange

  shouldComponentUpdate: (nextProps, nextState) ->
    dom = @getDOMNode()
    createGraph dom, nextProps, nextState
    false

  render: ->
    <div />

  _onNodeStoreChange: ->
    @setState
      nodes: NodeStore.getNodesMap()
      links: NodeStore.getLinks()

module.exports = Graph
