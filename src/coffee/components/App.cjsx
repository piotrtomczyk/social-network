React = require 'react'

Graph = require './Graph.cjsx'
LoginSection = require './LoginSection.cjsx'
NodeList = require './NodeList.cjsx'
UserActions = require '../actions/UserActions.coffee'
UserStore = require '../stores/UserStore.coffee'

App = React.createClass
  displayName: 'App'
  getInitialState: ->
    user: UserStore.getUser()
  componentWillMount: ->
    UserStore.addChangeListener @_onUserStoreChange
  componentDidMount: ->
    UserActions.fetch()
  componentWillUnmount: ->
    UserStore.removeChangeListener @_onUserStoreChange
  render: ->

    services = @_services
    list = services.map (service) =>
      <LoginSection service={service.name} link={service.code} user={@state.user[service.code]} />

    <div>
      <ul style={@_menuStyle()}>
        {list}
      </ul>
      <Graph />
      <NodeList services={services} />
    </div>

  _services:
    [
      { code: 'fb', name: 'Facebook' }
      { code: 'ig', name: 'Instagram' }
      { code: 'tt', name: 'Twitter' }
    ]
  _menuStyle: ->
    listStyle: 'none'
    position: 'absolute'
  _onUserStoreChange: ->
    @setState user: UserStore.getUser()

module.exports = App
