express = require 'express'
request = require 'superagent'

facebook = require '../controllers/facebook'
instagram = require '../controllers/instagram'
twitter = require '../controllers/twitter'

router = express.Router()

router.get '/user', (req, res) ->
  res.json req.user || {}

router.get '/tt/statuses', (req, res, next) ->
  id = req.query.id or req.user.tt.id
  twitter.statuses id
  .then res.json.bind(res), res.json.bind(res)

router.get '/tt/tags', (req, res, next) ->
  id = req.query.id
  twitter.tags id
  .then res.json.bind(res), res.json.bind(res)

router.get '/tt/friends', (req, res, next) ->
  id = req.query.id or req.user.tt.id
  twitter.friends id
  .then res.json.bind(res), res.json.bind(res)

router.get '/fb//tags', (req, res, next) ->
  id = req.query.id
  query =
    access_token: req.user.fb.token
  facebook.tags id, query
  .then res.json.bind(res), res.json.bind(res)

router.get '/fb/statuses', (req, res, next) ->
  id = req.query.id or req.user.fb.id
  query =
    access_token: req.user.fb.token
  facebook.statuses id, query
  .then res.json.bind(res), res.json.bind(res)

router.get '/fb/user', (req, res, next) ->
  id = req.query.id or req.user.fb.id
  query =
    access_token: req.user.fb.token
  facebook.user id, query
  .then res.json.bind(res), res.json.bind(res)

router.get '/fb/id', (req, res, next) ->
  id = req.query.id or req.user.fb.id
  facebook.id id
  .then res.json.bind(res), res.json.bind(res)

router.get '/fb/friends', (req, res, next) ->
  id = req.query.id or req.user.fb.id
  facebook.friends id
  .then res.json.bind(res), res.json.bind(res)

router.get '/ig/statuses', (req, res, next) ->
  id = req.query.id or req.user.ig.id
  instagram.media id, req.user.ig.token
  .then res.json.bind(res), res.json.bind(res)

router.get '/ig/tags', (req, res, next) ->
  id = req.query.id
  instagram.tags id, req.user.ig.token
  .then res.json.bind(res), res.json.bind(res)

router.get '/ig/friends', (req, res, next) ->
  id = req.query.id or req.user.ig.id
  instagram.follows id, req.user.ig.token
  .then res.json.bind(res), res.json.bind(res)

module.exports = router
