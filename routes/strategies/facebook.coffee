Facebook = (require 'passport-facebook').Strategy
passport = require 'passport'

Config = require '../../config.json'
common = require '../../common'

User = common.db.get 'user'

reuseUser = (user, fb) ->
  query = _id: user._id
  User.findAndModifyAsync query, $set: fb: fb

createUser = (fb) ->
  User.insertAsync fb: fb

findOrCreateUser = (fb) ->
  User.findOneAsync 'fb.id': fb.id
  .then (user) ->
    if user
      user
    else
      createUser fb

auth = (req, token, refreshToken, profile, done) ->

  success = (user) -> done null, user
  failure = (err) -> done err

  fb =
    id: profile.id
    token: token
    displayName: profile.displayName

  if req.user
    promise = reuseUser req.user, fb
  else
    promise = findOrCreateUser fb

  promise
  .then success, failure
  .catch failure

module.exports = new Facebook
  clientID: Config.FB_APP_ID
  clientSecret: Config.FB_APP_SECRET
  callbackURL: "#{Config.HOME_URL}/auth/fb/callback"
  passReqToCallback: true
  auth
