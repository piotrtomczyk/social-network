Twitter = (require 'passport-twitter').Strategy
passport = require 'passport'

Config = require '../../config.json'
common = require '../../common'

User = common.db.get 'user'

reuseUser = (user, tt) ->
  query = _id: user._id
  User.findAndModifyAsync query, $set: tt: tt

createUser = (tt) ->
  User.insertAsync tt: tt

findOrCreateUser = (tt) ->
  User.findOneAsync 'tt.id': tt.id
  .then (user) ->
    if user
      user
    else
      createUser tt

auth = (req, token, tokenSecret, profile, done) ->

  success = (user) -> done null, user
  failure = (err) -> done err

  tt =
    id: profile.id
    token: token
    displayName: profile.displayName

  if req.user
    promise = reuseUser req.user, tt
  else
    promise = findOrCreateUser tt

  promise
  .then success, failure
  .catch failure

module.exports = new Twitter
  consumerKey: Config.TWITTER_API_KEY
  consumerSecret: Config.TWITTER_API_SECRET
  callbackURL: "#{Config.HOME_URL}/auth/tt/callback"
  passReqToCallback: true
  auth
