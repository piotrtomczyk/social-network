Instagram = (require 'passport-instagram').Strategy
passport = require 'passport'

Config = require '../../config.json'
common = require '../../common'

User = common.db.get 'user'

reuseUser = (user, ig) ->
  query = _id: user._id
  User.findAndModifyAsync query, $set: ig: ig

createUser = (ig) ->
  User.insertAsync ig: ig

findOrCreateUser = (ig) ->
  User.findOneAsync 'ig.id': ig.id
  .then (user) ->
    if user
      user
    else
      createUser ig

auth = (req, token, tokenSecret, profile, done) ->

  success = (user) -> done null, user
  failure = (err) -> done err

  ig =
    id: profile.id
    token: token
    displayName: profile.displayName

  if req.user
    promise = reuseUser req.user, ig
  else
    promise = findOrCreateUser ig

  promise
  .then success, failure
  .catch failure

module.exports = new Instagram
  clientID: Config.IG_CLIENT_ID
  clientSecret: Config.IG_CLIENT_SECRET
  callbackURL: "#{Config.HOME_URL}/auth/ig/callback"
  passReqToCallback: true
  auth
