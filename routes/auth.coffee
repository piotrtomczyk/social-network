express = require 'express'
passport = require 'passport'

common = require '../common'

router = express.Router()

User = common.db.get 'user'

passport.use require './strategies/facebook.coffee'
passport.use require './strategies/instagram.coffee'
passport.use require './strategies/twitter.coffee'

passport.serializeUser (user, done) ->
  done null, user._id

passport.deserializeUser (id, done) ->
  User.findByIdAsync id
  .then (user) -> done null, user
  .catch (err) -> done err

router.get '/fb', passport.authenticate 'facebook',
  scope: ['user_status', 'read_stream']
router.get '/fb/callback',
  passport.authenticate 'facebook',
    successRedirect: '/'
    failureRedirect: '/'

router.get '/ig', passport.authenticate 'instagram'
router.get '/ig/callback',
  passport.authenticate 'instagram',
    successRedirect: '/'
    failureRedirect: '/'

router.get '/tt', passport.authenticate 'twitter'
router.get '/tt/callback',
  passport.authenticate 'twitter',
    successRedirect: '/'
    failureRedirect: '/'

module.exports = router
