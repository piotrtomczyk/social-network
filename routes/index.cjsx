express = require 'express'
React = require 'react'
ReactDOM = require 'react-dom/server'

App = require '../src/coffee/components/App.cjsx'

router = express.Router()

router.get '/', (req, res) ->
  component = ReactDOM.renderToString <App />
  res.render 'index', body: component

module.exports = router
