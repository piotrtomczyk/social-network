ConnectMongoskin = require 'connect-mongoskin'
Promise = require 'bluebird'
bodyParser = require 'body-parser'
cookieParser = require 'cookie-parser'
express = require 'express'
favicon = require 'serve-favicon'
logger = require 'morgan'
monk = require 'monk'
passport = require 'passport'
path = require 'path'
session = require 'express-session'

common = require './common'
common.db = db = monk 'localhost/snet'

Promise.promisifyAll require 'superagent'
Promise.promisifyAll db.get 'user'

api = require './routes/api'
auth = require './routes/auth'
routes = require './routes/index'

app = express()

if 'development' == app.get 'env'
  app.locals.pretty = true
  app.set 'json spaces', 2

app.set 'views', path.join __dirname, 'views'
app.set 'view engine', 'jade'

app.use logger 'dev'
app.use bodyParser.json()
app.use bodyParser.urlencoded extended: false
app.use cookieParser()
app.use session
  name: 'sid'
  resave: true
  saveUninitialized: false
  secret: 'app secret'
  store: new ConnectMongoskin db.driver
app.use passport.initialize()
app.use passport.session name: 'session'
app.use express.static path.join __dirname, 'build'

app.use '/', routes
app.use '/auth', auth
app.use '/api', api

app.use (req, res, next) ->
  err = new Error 'Not Found'
  err.status = 404
  next err

if 'development' == app.get 'env'
  app.use (err, req, res, next) ->
    res.status err.status or 500
    res.render 'error',
      message: err.message
      error: err

app.use (err, req, res, next) ->
  res.status err.status or 500
  res.render 'error',
    message: err.message
    error: {}

module.exports = app
