path = require 'path'
spawn = require('child_process').spawn

gulp = require 'gulp'
gutil = require 'gulp-util'

plumber = require 'gulp-plumber'
rename = require 'gulp-rename'

browserify = require 'browserify'
cjsxify = require 'cjsxify'
source = require 'vinyl-source-stream'
sync = require 'browser-sync'
transform = require 'vinyl-transform'

BACK_FILES = [
  'app.coffee'
  'controllers/**/*.coffee'
  'routes/**/*'
  'src/coffee/**/*.cjsx'
  'src/coffee/**/*.coffee'
  ]
FRONT_FILES = ['src/coffee/**/*.coffee', 'src/coffee/**/*.cjsx']
VIEWS_FILES = ['views/**/*.jade']

gulp.task 'sync', ->
  sync.init
    port: 5000 
    proxy: 'localhost:3000'
    ghostMode: false
    browser: []

gulp.task 'reload', ->
  sync.reload()

gulp.task 'js-reload', ['js'], ->
  sync.reload()

gulp.task 'js', (done) ->
  browserify './src/coffee/app.cjsx'
  .external 'bluebird'
  .external 'd3'
  .external 'events'
  .external 'flux'
  .external 'immutable'
  .external 'object-assign'
  .external 'react'
  .external 'superagent'
  .transform cjsxify
  .bundle()
  .pipe source 'bundle.js'
  .pipe gulp.dest 'build/js'

gulp.task 'vendor', (done) ->
  browserify()
  .require 'bluebird'
  .require 'd3'
  .require 'events'
  .require 'flux'
  .require 'immutable'
  .require 'object-assign'
  .require 'react'
  .require 'superagent'
  .bundle()
  .pipe source 'vendor.js'
  .pipe gulp.dest 'build/js'

gulp.task 'watch', ['js', 'server'], ->
  gulp.watch FRONT_FILES, ['js-reload']
  gulp.watch VIEWS_FILES, ['reload']
  gulp.watch BACK_FILES, ['server']

node = null
gulp.task 'server', ->
  node.kill() if node
  node = spawn 'node', ['bin/www'], stdio: 'inherit'
  node.on 'close', (code) ->
    if code == 8
      gutil.log 'error detected'

gulp.task 'default', ['watch', 'sync'], ->

process.on 'exit', ->
  node.kill() if node
